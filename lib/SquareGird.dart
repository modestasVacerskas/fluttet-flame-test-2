import 'dart:ui';
import 'myGame.dart';
import "square.dart";
import 'package:flutter/gestures.dart';


class SquareGrid
{
  final MyGame game;
  int nubmerOfSquaresRows  = 9;
  int nubmerOfSquaresInRow  = 9;

  int colorint;
  List<List<Square>> squaresGrid; 
  List<Square> squaresRows;

  SquareGrid(this.game)
  {
    initialiaze();
  }

  void initialiaze() async
  {
  
    squaresGrid = new List<List<Square>>();
    for(int i = 0;i < nubmerOfSquaresRows; i++)
    {
      squaresRows = new List<Square>();

      for(int j = 0; j < nubmerOfSquaresInRow; j++)
      {
        squaresRows.add(new Square(this.game, i, j));
      }
      squaresGrid.add(squaresRows);
    }
    
  }
  
  void render(Canvas c) 
  {
    for(int i = 0; i < 9; i++ )
    {
      for(int j = 0; j < 9; j++ )
      {
        c.drawRect(squaresGrid.elementAt(i).elementAt(j).square, squaresGrid.elementAt(i).elementAt(j).squareCollor);
      }
    }
    
  }

  void update(double t) 
  {
    for(int i = 0; i < 9; i++ )
    {
      for(int j = 0; j < 9; j++ )
      {
        squaresGrid.elementAt(i).elementAt(j).update(t);
      }
    }

  }

  void onTapDown(TapDownDetails d) 
  {
    print("TAP DOWN INSADE SQUARE GRID");
      for(int i = 0; i < 9; i++ )
      {
        for(int j = 0; j < 9; j++ )
        {
         if(squaresGrid.elementAt(i).elementAt(j).square.contains(d.globalPosition))
         {
           squaresGrid.elementAt(i).elementAt(j).onTapDown();
         }
        }
      }
  }

  void randomColorForRandomSquare(int randomRow, int randomCollum, int randomColor)
  {
      squaresGrid.elementAt(randomRow).elementAt(randomCollum).changeSquareColorCallable(randomColor);
  }
}