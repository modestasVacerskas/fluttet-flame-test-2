import 'dart:ui';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'dart:math';
import 'package:flutter/gestures.dart';
import 'square.dart';
import "topFile.dart";
import "SquareGird.dart";


class MyGame extends Game {
  Size screenSize;
  double tileSize;
  GUI gui;
  SquareGrid table;

  Random rnd;

  MyGame()
  {
    initialiaze();
  }

  void initialiaze() async
  {
    resize(await Flame.util.initialDimensions());
    gui = new GUI();
    rnd = Random();
    table = new SquareGrid(this);
    gui.initializeTimer();

  }


  void render(Canvas canvas) 
  {
    Rect bgRect = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    Paint bgPaint = Paint();
    bgPaint.color = Color(0xff576574);
    canvas.drawRect(bgRect, bgPaint); 
    
    table.render(canvas);
    
  }

  void update(double t) 
  {
    gui.countingTime();
    if(gui.changeColor == true)
    {
      if(gui.currentLevel ==  1)
      {
        int randomNumber = rnd.nextInt(5);      

        for(int i = 0; i < randomNumber; i++)
        {
          int randomRow = rnd.nextInt(9);
          int randomCollum = rnd.nextInt(9);
          int randomColor =  rnd.nextInt(5);
          table.randomColorForRandomSquare(randomRow, randomCollum, randomColor);        
          gui.changeColor = false;
        }       
      }
    }
    table.update(t);
  }

  void resize(Size size) 
  {
    screenSize = size;
    tileSize = screenSize.width / 9;
  }


  void onTapDown(TapDownDetails d)
  {
     table.onTapDown(d);
  }
}