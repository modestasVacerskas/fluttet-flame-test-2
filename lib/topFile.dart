import 'dart:ui';
import 'myGame.dart';
import 'dart:async';

class GUI
{
  int health = 3;
  int currentLevel = 1;
  int seconds = 5;
  int currentTime = 0;
  int currentColorNumber = 1;
  String timeInString;
  Timer timer;
  bool changeColor = false;
  Rect squareTime;
 
 
  initializeTimer()
  {
     timer = Timer.periodic(Duration(seconds: 1), (timer)
     {
      currentTime++;
      setTimeString();
     });
  }
  
  

  countingTime()
  {
    if(currentTime >= seconds)
    {
      changeColor = true;
      currentTime = 0;
    }
    setTimeString();
    
  }

  setTimeString()
  {
    switch(currentTime)
    {
      case 1:
      timeInString = "1";
      break;
      case 2:
      timeInString = "2";
      break;
      case 3:
      timeInString = "3";
      break;
      case 4:
      timeInString = "4";
      break;
      case 5:
      timeInString = "5";
      break;
      default:
      timeInString = "0";
      break;
    }
  }

}